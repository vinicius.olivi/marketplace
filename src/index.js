import React from "react";
import ReactDOM from "react-dom";

//PAGES
import Cadastro from "./pages/cadastro";

//STYLE
import "./styles/global.css";

ReactDOM.render(
  <React.StrictMode>
    <Cadastro />
  </React.StrictMode>,
  document.getElementById("root"),
);
